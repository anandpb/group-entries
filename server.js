const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
var fs = require("fs");
var XLSX = require("xlsx");
const stopwords = require('stopword');

var changeViewFile = 'fileWrites/changeViewFile.txt';
fs.writeFileSync(changeViewFile, '');

var manualList = ['please', 'pls'];

var filename = 'Metrics Dashboard_Weekly Support Metrics_Table.xlsx';
var workbook = XLSX.readFile(filename);
var worksheet = workbook.Sheets[workbook.SheetNames[0]];
var result = XLSX.utils.sheet_to_json(worksheet, { raw: true });
// console.log(result);

var sortedNgramsG = null;

const app = express();
const PORT = 3000;

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.get('/fetch-list', async (req, res) => {

    // const ngramLengths = [1, 2, 3, 4, 5]; // specify n values for ngrams
    const ngramLengths = [3]; // specify n values for ngrams
    
    var srcList = [];
    var allList = [];
    
    // put all data from the column 'Issue Reported' in an array
    result.forEach((row) => {
        // fs.appendFileSync(origContent, `${row['Issue Reported']}\n`);
        srcList.push(row['Issue Reported']);
    });
    
    srcList.forEach((content) => {
        const lcContent = content.replace(/[^\w\s]/g, '').toLowerCase(); // remove punctuation and change full content to lowercase
        // const lcContent = content.toLowerCase(); // change full content to lowercase
    
        // Split text into individual words
        const wordsList = lcContent.split(/\s+/);
    
        // Remove stop words
        const remStopWords = stopwords.removeStopwords(wordsList);
    
        const remLesTwoWords = remStopWords.filter(element => element.length > 2); // remove those with less than 3 letters    
    
        const filteredWords = remLesTwoWords.filter(element => !(manualList.includes(element))); // remove words using manualList
    
        var ngramsLis = generateNgramsObject(filteredWords);
    
        allList.push(ngramsLis); // row by row ngrams list     
    });
    
    // HERE
    // console.log(allList);
    console.log('no. of entries read', allList.length);
    
    var ngramCountsList = [{}, {}, {}, {}, {}];
    
    for (let k = 0; k < ngramLengths.length; k++) {
        allList.forEach((outerList) => {
            for (let i = 0; i < outerList.length; i++) {
                if (i === k) {
                    outerList[i].forEach((ngram) => {
                        if (!(ngramCountsList[k])[ngram[0]]) {
                            (ngramCountsList[k])[ngram[0]] = 0;
                        }
                        (ngramCountsList[k])[ngram[0]]++;
                    });
                }            
            }
        }); 
    }
    
    var sortedNgrams = [];
    
    for (let i = 0; i < ngramLengths.length; i++) {
    
        // console.log(Object.keys(ngramCountsList[i]).length);
        sortedNgrams[i] = Object.entries(ngramCountsList[i]).sort((a, b) => b[1] - a[1]).slice(0, 20);
    }

    // sortedNgrams.forEach((sortedNgram) => {
    //     sortedNgram.forEach((ngramnCount) => {
    //         fs.appendFileSync(changeViewFile, `${ngramnCount}\n`);
    //     });
    
    //     fs.appendFileSync(changeViewFile, '\n');
    // });

    sortedNgramsG = sortedNgrams;
    // Stringify the sortedNgrams variable
    res.send(JSON.stringify(sortedNgrams));
    
    // HERE
    
    function generateNgramsObject(fileWords) {
    
        var ngramsList = [];
    
        // Generate n-grams *
    
        ngramLengths.forEach((n) => {
            const ngramCounts = {};
    
            // console.log(fileWords.length);
            for (let i = 0; i < fileWords.length - n + 1; i++) {
                const ngram = fileWords.slice(i, i + n).join(' ');
    
                if (!ngramCounts[ngram]) {
                    ngramCounts[ngram] = 0;
                }
                ngramCounts[ngram]++;
            }
    
            // Sort n-grams by frequency
            const ngramsColl = Object.entries(ngramCounts);
    
            ngramsList.push(ngramsColl);
    
        });
    
        return ngramsList;
    }    
});

app.get('/fetch-table', async (req, res) => {

    // res.send(result);

    var resTableObj = {};

    var sortedNgramsRed = sortedNgramsG[0]; // sorted ngrams reduced
    
    // var i = 1;
    sortedNgramsRed.forEach((ngram) => {
        // console.log(typeof ngram);
        var tempEntries = [];
        result.forEach((row) => {
            // console.log('inside row by row loop', i++);
            // console.log(typeof row['Issue Reported'].toLowerCase());

            const lcContent = row['Issue Reported'].replace(/[^\w\s]/g, '').toLowerCase(); // remove punctuation and convert to lowercase
            // const lcContent = row['Issue Reported'].toLowerCase(); // convert to lowercase
            
            const wordsList = lcContent.split(/\s+/); // Split text into individual words
        
            const remStopWords = stopwords.removeStopwords(wordsList); // Remove stop words
        
            const remLesTwoWords = remStopWords.filter(element => element.length > 2); // remove those with less than 3 letters    
        
            const filteredWords = remLesTwoWords.filter(element => !(manualList.includes(element)));

            const filteredString = filteredWords.join(' ');
            
            if (filteredString.includes(ngram[0])) {
                // console.log('true', i++);
                tempEntries.push(Object.entries(row));
            }    
        });
        // console.log(tempEntries.length);
        resTableObj[ngram[0]] = tempEntries;
    });

    // console.log(Object.keys(resTableObj).length);
    res.send(resTableObj);

});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});